#!/bin/bash 

# ssh_fs.sh - script para montar una carpeta remota en la maquina local

# remote machine params
USER="username"
ADDR="000.000.000.000" 
REMOTE="/home/user/folder/" # remote machine folder

# local machine params
LOCAL="REMOTE"

FLAGS=""

sudo sshfs -o allow_other,defer_permissions \
  ${USER}@${ADDR}:${REMOTE} ${LOCAL}

exit 0
